class apache::config {
  file { '/etc/apache2/apache2.conf':
    ensure  => file,
    source  => 'puppet:///modules/apache/apache2.conf',
    require => Package['apache2'],
    notify  => Service['apache2'],
  }

  service { 'apache2':
    ensure => running,
    enable => true,
  }
}

