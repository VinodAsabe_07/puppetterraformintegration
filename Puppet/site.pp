node 'default' {
    class { 'apache': }
}

class apache {
    package { 'httpd':
        ensure => installed,
    }

    service { 'httpd':
        ensure  => 'running',
        enable  => true,
        require => Package['httpd'],
    }

    file { 'C:/Apache24/htdocs/index.html':
        ensure  => present,
        content => '<h1>Hello, Puppet and Terraform on Windows!</h1>',
        require => Service['httpd'],
    }
}
